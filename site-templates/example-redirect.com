#Let's redirect http and www requests to non-www site.
server {
  # don't forget to tell on which port this server listens
  listen 80;
  listen [::]:80 ipv6only=on;
  
  # listen on the www host
  server_name [[URL]] [[FORWARDED_URL]];		# do no forget to replace example.com 
 
  # and redirect to the https host (declared below)
  return 301 https://[[URL]]$request_uri;		# do no forget to replace example.com 
}

#Let's redirect www https requests to non-www site.
server {
  # don't forget to tell on which port this server listens
  listen 443 ssl http2 deferred;			#HTTP2 Requires nginx 1.9.5+
  listen [::]:443 ssl http2 deferred ipv6only=on;
  
  # listen on the www host
  server_name [[FORWARDED_URL]];
 
  # and redirect to the https host (declared below)
  # do no forget to replace example.com 
  return 301 https://[[URL]]$request_uri;
}
